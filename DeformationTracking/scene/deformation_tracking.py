import Sofa
import sys
import numpy as np
import ConfigParser  

import random
import os

path_param = os.getcwd() #'/home/agniv/Code/sofa/applications/plugins/DeformationTracking/scene/'
sys.path.append(path_param)

import parameters
import force_manager


class SofaDeform(Sofa.PythonScriptController):
    count = 0
    properties_file = path_param+'/parameter.properties'
    config = ConfigParser.RawConfigParser()
    config.read(properties_file)
    param = parameters.Parameters()
    force  = force_manager.ForceManager()
    myMechanicalObjectPointer = 0
    tracker = 0
    current_node = 0
    force_nodes = []
    node_count = 0
    jacobian_phase = 0
    jacobian_toggle = 0
    jacobian_force = 0
    rest_velocity = 0
    obj_position = 0
    obj_normal = 0
    mesh_position = 0 
    prev_force = 0
    update_stage = 0

    def getFemProperties(self):
	return self.param.femProperties(self.config)

    def getTrackingProperties(self):
	return self.param.trackingProperties(self.config)

    def getTrackerMessage(self):
	return self.tracker.trackerMessage

    def spawnScene(self):
	node = self.rootNode.createChild('Fem_Simulation')
	mechanical_model, visual_model, rayleigh_mass, rayleigh_stiffness, iteration, young_modulus, poisson_ratio, fixed_constraint, max_force_vectors, self.jacobian_force = self.getFemProperties()
	node, self.myMechanicalObjectPointer = self.param.createScene(node, mechanical_model, visual_model, rayleigh_mass, rayleigh_stiffness, iteration, young_modulus, poisson_ratio, fixed_constraint, max_force_vectors)
	return node
    
    def onLoaded(self,node):
	print 'onLoaded'
        self.rootNode = node
         
    def createGraph(self,node):
	#print 'createGraph'
        node = self.spawnScene()
	node.init()
   	self.prepTracker(node)
	self.rootNode = node
	#print 'created graph'
        return 0

    def prepTracker(self,node):
	visual_model, init_transform, mechanical_model, data_folder, output_folder, c_x, c_y, f_x, f_y, config_path, cao_model_path, iterations = self.getTrackingProperties()
        self.tracker = node.createObject('DeformationTracking',name='deform_tracker',objFileName=visual_model,mechFileName=mechanical_model,transformFileName=init_transform,dataFolder=data_folder,outputDirectory=output_folder,C_x=c_x,C_y=c_y,F_x=f_x,F_y=f_y,configPath=config_path,caoModelPath=cao_model_path,objectModel='',iterations=iterations)
        self.tracker.simulationMessage = 'ready'

    def jacobian_deform(self):
	#print self.tracker.simulationMessage
	if(self.tracker.simulationMessage != 'applying_J'):
	    for i in range(0, len(self.tracker.forcePoints)):
	    	node_details = self.tracker.forcePoints[i]
	    	self.rootNode = self.force.append_nodes(self.rootNode, node_details[0], self.current_node)
		self.force_nodes.extend([self.current_node])
	    	self.current_node = self.current_node# + 1
	    self.tracker.simulationMessage = 'applying_J'
	    self.node_count = len(self.tracker.forcePoints)
	elif(self.tracker.simulationMessage == 'applying_J'):
	    #print 'reading force nodes'
	    self.jacobian_toggle,self.rest_velocity, self.obj_position, self.obj_normal, self.mesh_position, self.prev_force, self.rootNode, self.tracker = self.force.apply_force_J(self.force_nodes[self.node_count - 1], self.rootNode, self.jacobian_phase, self.jacobian_toggle,self.rest_velocity, self.obj_position, self.obj_normal, self.mesh_position, self.prev_force, self.jacobian_force, self.tracker)
	    self.jacobian_phase,self.node_count,self.jacobian_toggle = self.force.update_force_J_counters(self.jacobian_phase,self.node_count, self.jacobian_toggle)
	    if(self.node_count == 0):
		self.tracker.simulationMessage = 'jacobian_ready'

    def update(self):
	if(self.update_stage == 0):
	    self.tracker.simulationMessage = 'applying_update'
	    #print 'F^'
	    #print 'from Python: need to update force now'
	    #print self.tracker.update_tracker
	    self.node_count = len(self.tracker.forcePoints)
	    #print 'node count: '+str(self.node_count)
	    self.force.apply_force(self.force_nodes[self.node_count - 1], self.rootNode, self.tracker, self.prev_force)
	    self.update_stage = 1
	elif(self.update_stage == 1):	    
	    self.update_stage = 2
	    #print 'Fv'
	elif(self.update_stage == 2):
	    self.rest_velocity, self.obj_position, self.obj_normal, self.mesh_position, self.prev_force = self.force.pack_model(self.rootNode,self.tracker,self.force_nodes[0], self.rest_velocity, self.obj_position, self.obj_normal, self.mesh_position, self.prev_force)
	    self.update_stage = 0
	    self.tracker.simulationMessage = 'update_ready'
	    #print 'Fv'
	

    def onBeginAnimationStep(self,dt):
	#print 'start'
	#print "Animation step..."+str(self.count)
	self.count = self.count + 1
	#position = self.myMechanicalObjectPointer.findData('position').value
	if(self.getTrackerMessage() == "matched"):
	    self.jacobian_deform()
	elif(self.getTrackerMessage() == "updated"):
	    self.update()


