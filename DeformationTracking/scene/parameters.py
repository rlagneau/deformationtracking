import ConfigParser
import Sofa

class Parameters:

    def femProperties(self,config):
	fem_handle = 'fem_simulation'
        mechanical_model = config.get(fem_handle,'mechanical_model')
	visual_model = config.get(fem_handle,'visual_model')
	rayleigh_mass = config.get(fem_handle,'rayleigh_mass')
	rayleigh_stiffness = config.get(fem_handle,'rayleigh_stiffness')
	iteration = config.get(fem_handle,'iteration')
	young_modulus = config.get(fem_handle,'young_modulus')
	poisson_ratio = config.get(fem_handle,'poisson_ratio')
	fixed_constraint = config.get(fem_handle,'fixed_constraint')
	max_force_vectors = config.get(fem_handle,'max_force_vectors')
	jacobian_force = config.get(fem_handle,'jacobian_force')


	return mechanical_model, visual_model, rayleigh_mass, rayleigh_stiffness, iteration, young_modulus, poisson_ratio, fixed_constraint, max_force_vectors, jacobian_force

    def trackingProperties(self,config):
	tracker_handle = 'deformation_tracking'
        visual_model = config.get(tracker_handle,'visual_model')
 	init_transform = config.get(tracker_handle,'init_transform')
	mechanical_model = config.get(tracker_handle,'mechanical_model')
	data_folder = config.get(tracker_handle,'data_folder')
	output_folder = config.get(tracker_handle,'output_folder')
	c_x = config.get(tracker_handle,'c_x')
	c_y = config.get(tracker_handle,'c_y')
	f_x = config.get(tracker_handle,'f_x')
	f_y = config.get(tracker_handle,'f_y')
	config_path = config.get(tracker_handle,'config_path')
	cao_model_path = config.get(tracker_handle,'cao_model_path')
	iterations = config.get(tracker_handle,'iterations')

	return visual_model, init_transform, mechanical_model, data_folder, output_folder, c_x, c_y, f_x, f_y, config_path, cao_model_path, iterations


    def createScene(self,node,mechanical_model, visual_model, rayleigh_mass, rayleigh_stiffness, iteration, young_modulus, poisson_ratio, fixed_constraint, max_force_vectors):
	node.createObject('MeshVTKLoader',name="meshLoader",filename=mechanical_model)
        #node.createObject('EulerImplicitSolver',rayleighMass=rayleigh_mass,rayleighStiffness=rayleigh_stiffness)
	node.createObject('StaticSolver',rayleighMass=rayleigh_mass,rayleighStiffness=rayleigh_stiffness)
        node.createObject('CGLinearSolver',iterations=iteration,tolerance=0.001,threshold=0.001)
	node.createObject('TetrahedronSetTopologyContainer',name="topo",src="@meshLoader")
	node.createObject('MechanicalObject',name='myMech',scale="1",position="@meshLoader.position",tags="NoPicking")
	node.createObject('TetrahedronSetGeometryAlgorithms',template="Vec3d",name="GeomAlgo")
	node.createObject('DiagonalMass',name="computed using mass density",massDensity="1")
        node.createObject('SphereModel',radius='0.5', group='1')
	node.createObject('FixedConstraint',name='FixedConstraint',indices=fixed_constraint)
	node.createObject('FastTetrahedralCorotationalForceField',template='Vec3d',name='FEM',method='large',poissonRatio=poisson_ratio,youngModulus=young_modulus,computeGlobalMatrix='0')
        vizNode = node.createChild('Visu')
        vizNode.createObject('OglModel',name='Visual',fileMesh=visual_model)
        vizNode.createObject('BarycentricMapping',input='@..',output='@Visual')


	for i in range(0,int(max_force_vectors)):
	    node.createObject('LinearForceField',name=str(i),points='0',forces='0 0 0',times='0')

	return node, vizNode.getObject('Visual')
